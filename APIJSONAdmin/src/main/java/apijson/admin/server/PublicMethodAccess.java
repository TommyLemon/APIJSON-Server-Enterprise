package apijson.admin.server;

import static zuo.biao.apijson.RequestRole.ADMIN;
import static zuo.biao.apijson.RequestRole.CIRCLE;
import static zuo.biao.apijson.RequestRole.CONTACT;
import static zuo.biao.apijson.RequestRole.LOGIN;
import static zuo.biao.apijson.RequestRole.OWNER;

import java.lang.annotation.Inherited;

import zuo.biao.apijson.MethodAccess;

@MethodAccess(
		GET = {LOGIN, CONTACT, CIRCLE, OWNER, ADMIN},
		HEAD = {LOGIN, CONTACT, CIRCLE, OWNER, ADMIN},
		POST = {OWNER, ADMIN},
		PUT = {OWNER, ADMIN},
		DELETE = {OWNER, ADMIN}
		)
@Inherited
public @interface PublicMethodAccess {
}
