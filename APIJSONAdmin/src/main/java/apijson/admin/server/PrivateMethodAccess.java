package apijson.admin.server;

import static zuo.biao.apijson.RequestRole.ADMIN;
import static zuo.biao.apijson.RequestRole.OWNER;

import java.lang.annotation.Inherited;

import zuo.biao.apijson.MethodAccess;

@MethodAccess(
		GET = {OWNER, ADMIN},
		HEAD = {OWNER, ADMIN},
		POST = {OWNER, ADMIN},
		PUT = {OWNER, ADMIN},
		DELETE = {OWNER, ADMIN}
		)
@Inherited
public @interface PrivateMethodAccess {
}
