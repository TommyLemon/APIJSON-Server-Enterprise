/*Copyright ©2016 TommyLemon(https://github.com/TommyLemon/APIJSON)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.*/

package apijson.admin.server.model;

import static zuo.biao.apijson.RequestRole.ADMIN;
import static zuo.biao.apijson.RequestRole.OWNER;

import java.io.Serializable;
import java.sql.Timestamp;

import zuo.biao.apijson.MethodAccess;

/**订单
 * @author Lemon
 */
//不起作用 @PrivateMethodAccess
@MethodAccess(
		GET = {OWNER, ADMIN},
		HEAD = {OWNER, ADMIN},
		POST = {OWNER, ADMIN},
		PUT = {OWNER, ADMIN},
		DELETE = {OWNER, ADMIN}
		)
public class Order implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long id; //唯一主键
	private Long userId; //用户id
	private Long goodsId; //商品id
	private Integer state; //状态：0-已下单 1-已支付
	private Integer count; //购买数量
	private Integer price; //付款金额
	private Timestamp date; //创建时间
	private Timestamp updateTime; //更新时间
	private String detail; //详细介绍
	private Long startTime; //
	private String endTime; //


	public Order() {
		super();
	}
	public Order(long id) {
		this();
		setId(id);
	}




	public Long getId() {
		return id;
	}

	public Order setId(Long id) {
		this.id = id;
		return this;
	}

	public Long getUserId() {
		return userId;
	}

	public Order setUserId(Long userId) {
		this.userId = userId;
		return this;
	}

	public Long getGoodsId() {
		return goodsId;
	}

	public Order setGoodsId(Long goodsId) {
		this.goodsId = goodsId;
		return this;
	}

	public Integer getState() {
		return state;
	}

	public Order setState(Integer state) {
		this.state = state;
		return this;
	}

	public Integer getCount() {
		return count;
	}

	public Order setCount(Integer count) {
		this.count = count;
		return this;
	}

	public Integer getPrice() {
		return price;
	}

	public Order setPrice(Integer price) {
		this.price = price;
		return this;
	}

	public Timestamp getDate() {
		return date;
	}

	public Order setDate(Timestamp date) {
		this.date = date;
		return this;
	}

	public Timestamp getUpdateTime() {
		return updateTime;
	}

	public Order setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
		return this;
	}

	public String getDetail() {
		return detail;
	}

	public Order setDetail(String detail) {
		this.detail = detail;
		return this;
	}

	public Long getStartTime() {
		return startTime;
	}

	public Order setStartTime(Long startTime) {
		this.startTime = startTime;
		return this;
	}

	public String getEndTime() {
		return endTime;
	}

	public Order setEndTime(String endTime) {
		this.endTime = endTime;
		return this;
	}


}