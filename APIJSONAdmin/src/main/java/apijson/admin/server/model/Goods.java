/*Copyright ©2016 TommyLemon(https://github.com/TommyLemon/APIJSON)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.*/

package apijson.admin.server.model;

import static zuo.biao.apijson.RequestRole.ADMIN;
import static zuo.biao.apijson.RequestRole.CIRCLE;
import static zuo.biao.apijson.RequestRole.CONTACT;
import static zuo.biao.apijson.RequestRole.LOGIN;
import static zuo.biao.apijson.RequestRole.OWNER;

import java.io.Serializable;
import java.sql.Timestamp;

import zuo.biao.apijson.MethodAccess;

/**商品
 * @author Lemon
 */
//不起作用 @PublicMethodAccess
@MethodAccess(
		GET = {LOGIN, CONTACT, CIRCLE, OWNER, ADMIN},
		HEAD = {LOGIN, CONTACT, CIRCLE, OWNER, ADMIN},
		POST = {OWNER, ADMIN},
		PUT = {OWNER, ADMIN},
		DELETE = {OWNER, ADMIN}
		)
public class Goods implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long id; //主键
	private String name; //名称
	private Integer price; //价格
	private Integer count; //可用次数：0-不限
	private Integer duration; //有效期，精确到月：0-不限
	private Timestamp date; //创建时间
	private String detail; //详细介绍


	public Goods() {
		super();
	}
	public Goods(long id) {
		this();
		setId(id);
	}




	public Long getId() {
		return id;
	}

	public Goods setId(Long id) {
		this.id = id;
		return this;
	}

	public String getName() {
		return name;
	}

	public Goods setName(String name) {
		this.name = name;
		return this;
	}

	public Integer getPrice() {
		return price;
	}

	public Goods setPrice(Integer price) {
		this.price = price;
		return this;
	}

	public Integer getCount() {
		return count;
	}

	public Goods setCount(Integer count) {
		this.count = count;
		return this;
	}

	public Integer getDuration() {
		return duration;
	}

	public Goods setDuration(Integer duration) {
		this.duration = duration;
		return this;
	}

	public Timestamp getDate() {
		return date;
	}

	public Goods setDate(Timestamp date) {
		this.date = date;
		return this;
	}

	public String getDetail() {
		return detail;
	}

	public Goods setDetail(String detail) {
		this.detail = detail;
		return this;
	}


}